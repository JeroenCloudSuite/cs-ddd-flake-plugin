new_style_union = """
@dataclass(frozen=True)
class ActivateBasket(Command):
    basket_id: UUID | None
"""

new_style_union_tripple = """
@dataclass(frozen=True)
class ActivateBasket(Command):
    basket_id: UUID | None | str"""

old_scool_union = """
@dataclass(frozen=True)
class ActivateBasket(Command):
    basket_id: Union[UUID, None]"""

normal_class = """
@dataclass(frozen=True)
class ActivateBasket(Query):
    basket_id: UUID
"""

code_with_function = """
@dataclass(frozen=True)
class ActivateBasket(Command):
    basket_id: Union[UUID, None]

def test():
    pass"""

subscript_command = """
class DoStuff(Command[int]):
    id: int | None
"""

incorrect_api_dto = """
@dataclass(frozen=True)
class UpToDate(ApiDTO):
    id: int
    name: str | Label
"""

correct_api_dto = """
@dataclass(frozen=True)
class UpToDate(ApiDTO):
    id: int
    name: Union[str, Label]
"""

in_correct_response = """
@dataclass(frozen=True)
class UpToDate(Response):
    id: int
    name: str | Label"""

correct_response = """
@dataclass(frozen=True)
class UpToDate(Response):
    id: int
    name: Union[str, Label]"""
