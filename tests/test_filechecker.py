from unittest import TestCase
from tests.cases import (
    new_style_union,
    normal_class,
    old_scool_union,
    code_with_function,
    new_style_union_tripple,
    subscript_command,
    incorrect_api_dto,
    correct_api_dto,
    correct_response,
    in_correct_response,
)
from ddd_plugin import Plugin
from ast import parse


def _result(source: str) -> set[str]:
    tree = parse(source)
    plugin = Plugin(tree)
    return {msg for _, _, msg, _ in plugin.run()}


class TestFileChecker(TestCase):
    def test_filechecker_fail(self):
        result = _result(new_style_union)
        self.assertSetEqual(result, {Plugin.msg})

    def test_filechecker_pass(self):
        result = _result(normal_class)
        self.assertSetEqual(result, set())

    def test_with_union(self):
        result = _result(old_scool_union)
        self.assertSetEqual(result, set())

    def test_with_functions(self):
        result = _result(code_with_function)
        self.assertSetEqual(result, set())

    def test_tricky_filechecker(self):
        result = _result(new_style_union_tripple)
        self.assertSetEqual(result, {Plugin.msg})

    def test_tricky_filechecker2(self):
        result = _result(subscript_command)
        self.assertSetEqual(result, {Plugin.msg})

    def test_api_dto(self):
        result = _result(incorrect_api_dto)
        self.assertSetEqual(result, {Plugin.msg})

    def test_correct_api_dto(self):
        result = _result(correct_api_dto)
        self.assertSetEqual(result, set())

    def test_correct_response(self):
        result = _result(correct_response)
        self.assertSetEqual(result, set())

    def test_response(self):
        result = _result(in_correct_response)
        self.assertSetEqual(result, {Plugin.msg})
