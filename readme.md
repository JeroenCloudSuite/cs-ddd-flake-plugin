# flake-8 cs-ddd plugin
A flake plugin for checking the issues with generated defs and new style unions.

For example writing a Query class with new style union
```python
class GetBasketIndex(Query[GetBasketIndexResponse]):
    basket_id: UUID | str
```
This could result in weird errors in the deserialization or serialization of the Query. What is allowed is to use a old style `Union`
```python
class GetBasketIndex(Query[GetBasketIndexResponse]):
    basket_id: Union[UUID, str]
```
This flake8 plugin checks this.

### installation
```sh
$ pip install git+https://gitlab.com/JeroenCloudSuite/cs-ddd-flake-plugin.git@v0.1.1
```
Or put it in the `requirements.txt` file
```
# requirements.txt 
flake8==4.0.1
flake8-cs-ddd @ git+https://gitlab.com/JeroenCloudSuite/cs-ddd-flake-plugin.git@v0.1.1
```

To verify the correct installation you can run the `flake8 --help` and you should see the plugin at the bottom
```sh
$ flake8 --help
usage: flake8 [options] file file ...
...
pyflakes:
  --builtins BUILTINS   define more built-ins, comma separated
  --doctests            also check syntax of the doctests

Installed plugins: flake8_cs_ddd: 0.1.1, mccabe: 0.7.0, pycodestyle: 2.11.1, pyflakes: 3.2.0
```