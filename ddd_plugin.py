from _ast import ClassDef
import ast
from typing import Any, Generator


class Vistor(ast.NodeVisitor):
    base_classes_to_check = {'Command', 'Query', 'ApiDTO', 'Response'}

    def __init__(self) -> None:
        self.problems: list[tuple[int, int]] = []

    def _check_base_class(self, node: ClassDef) -> bool:
        base_classes = {node.id for node in node.bases if hasattr(node, 'id')}.union(  # noqa E501
            {base.value.id for base in node.bases if isinstance(base, ast.Subscript) and hasattr(base.value,'id')})  # noqa E501
        return any(self.base_classes_to_check.intersection(base_classes))

    def visit_ClassDef(self, node: ClassDef) -> Any:
        if self._check_base_class(node):
            for field in node.body:
                if isinstance(field, ast.AnnAssign):
                    if isinstance(field.annotation, ast.BinOp):
                        self.problems.append((field.lineno, field.col_offset))
        self.generic_visit(node)


class Plugin:
    name = __name__
    version = "0.1.1"
    msg = "DDD001 cannot use new style union annotation use Union[] instead"

    def __init__(self, tree: ast.AST):
        self.tree = tree

    def run(self) -> Generator[tuple[int, int, str, type], None, None]:
        visitor = Vistor()
        visitor.visit(self.tree)
        for line_no, column_offset in visitor.problems:
            yield (
                line_no,
                column_offset,
                self.msg,
                type(self),
            )
