import setuptools

requires = [
    "flake8 > 3.0.0",
]


setuptools.setup(
    name="flake8_cs_ddd",
    license="MIT",
    version="0.1.1",
    description="Flake8 plugin to check for cs_ddd issues",
    author="Me",
    author_email="example@example.com",
    url="https://github.com/me/flake8_example",
    py_modules=["ddd_plugin"],
    install_requires=requires,
    entry_points={
        "flake8.extension": [
            "DDD = ddd_plugin:Plugin",
        ],
    },
    classifiers=[
        "Framework :: Flake8",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Software Development :: Quality Assurance",
    ],
)
